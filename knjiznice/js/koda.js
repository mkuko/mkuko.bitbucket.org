
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";





/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}





/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

function dodajSvojegaUporabnika(){
  dodajanjeUporabnika(0);
}

//CUSTOM x
function dodajanjeUporabnika(stUporabnika){
    var sessionId = getSessionId();

    var ime;
    var priimek;
    var datum;
    
       ime =  $("#ime").val();
       priimek =$("#priimek").val();
       datum =$("#datum").val();

    if(stUporabnika == 1){
     ime = "Joze";
     priimek = "Gorisek";
     datum = "1998-05-01";
    }else if(stUporabnika == 2){
      ime = "Mojca";
     priimek = "Novak";
     datum = "1990-03-02";
    }else if(stUporabnika == 3){
      ime = "Mile";
     priimek = "Kovač";
     datum = "1998-02-04";
    }
    if ((!$("#ime").val() || !$("#priimek").val() || datum.length !=10) && stUporabnika == 0) {
	  $("#dodajSporocilo").text("Polja ne smejo biti prazna, datum rojstva pa mora biti v pravi obliki! (npr: 2000-01-03)");
	} else {
        $.ajaxSetup({
            headers: {
            "Ehr-Session": sessionId
            }
        });
        
        $.ajax({
            url: baseUrl + "/ehr",
            type: 'POST',
            success: function (data) {
                var ehrId = data.ehrId;
                
                // build party data
                var partyData = {
                    firstNames:ime,
                    lastNames:priimek,
                    dateOfBirth:datum, 
                    partyAdditionalInfo: [
                        {
                            key: "ehrId",
                            value: ehrId
                        }
                    ]
                };
                
                
                
                
                
                $.ajax({
                    url: baseUrl + "/demographics/party",
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function (party) {
                        if (party.action == 'CREATE') {
                          if(stUporabnika == 0){
                        
	                          $("#dodajSporocilo").text("Uspesno dodajanje! Vaš EHR: " + ehrId);
	                          $("#meritevEHR").val(ehrId);
                          }else{
                            if(stUporabnika == 1){
                              eth1 = ehrId;
                                                             console.log("uporabnik1: ETH  " + eth1);
                              $("#generirajSporocilo").text("Uspesno kreiranje uporabnikov!");
                              $("#generiranEHR1").val(eth1);
                            } else if(stUporabnika == 2){
                              eth2 = ehrId;
                                                            console.log("uporabnik2: ETH  " + eth2);

                              $("#generirajSporocilo").text("Uspesno kreiranje uporabnikov!");
                              $("#generiranEHR2").val(eth2);
                            }else if(stUporabnika == 3){
                              eth3 = ehrId;
                                                            console.log("uporabnik3: ETH  " + eth3);

                              $("#generirajSporocilo").text("Uspesno kreiranje uporabnikov!");
                              $("#generiranEHR3").val(eth3);
                            }
                            
                          }
                        }
                    }
                });
            }
        });
	}
}

//-CUSTOM x


function izmeri(){
  meritevDodaj(0);
}



var meritevDodaj = function(stUporabnika){
      var sessionId = getSessionId();
      var ehrId;
      var visina;
      var teza;
      if(stUporabnika ==0){
      ehrId= document.getElementById('meritevEHR').value;
      
      visina = document.getElementById("meritevVisina").value;
      teza = document.getElementById("meritevTeza").value;
      }
      if(stUporabnika == 1){
        ehrId = eth1;
        visina = "175";
        teza = "50";
      }else if(stUporabnika == 2){
        ehrId = eth2;
        visina = 184;
        teza = 72;
      }else if(stUporabnika == 3){
        ehrId = eth3;
        visina = 150;
        teza = 68;
      }
      if(!ehrId || ehrId.length == 0 || visina.length == 0 || teza.length == 0){
          $("#meritevSporocilo").html("Prosim vnesite vse zahtevane podatke!");
      }else{
        $.ajaxSetup({
          headers: {"Ehr-Session": sessionId}
        });
        var podatki = {
  		    "ctx/language": "en",
  		    "ctx/territory": "SI",
  		    "vital_signs/height_length/any_event/body_height_length": visina,
  		    "vital_signs/body_weight/any_event/body_weight": teza
  		   	
		    };
        var parametri = {
          ehrId: ehrId,
          templateId: 'Vital Signs',
          format: 'FLAT',
          committer: 'aplikacija-selebritiITM'
        };
        $.ajax({
          url: baseUrl + "/composition?" + $.param(parametri),
          type: 'POST',
          contentType: 'application/json',
          data: JSON.stringify(podatki),
          success: function(res){
            if(stUporabnika == 0){
            $("#meritevSporocilo").html("Meritev uspesno dodana!");
            $("#primerjajEHR").val(ehrId);
            }
          },
          error: function(err){
            $("#meritevSporocilo").html("<b>Napaka!</b>")
          }
        });
      }
    };
    
    
    
    
    
    
    
    
    
    
    
    
    var eth1 ="54593d8d-6cc8-491b-83ca-86c2ecb7e97f"; 
    var eth2 ="2dbfcd86-b7d1-40b4-9a5c-4cbba3763fc2";
    var eth3="87f5ced0-3f01-4ace-b783-40a1d2624a6d";
    
    
    function meri(){
      meritevDodaj(1);
      meritevDodaj(2);
      meritevDodaj(3);
    }
    
    function generirajUporabnika1(){
      dodajanjeUporabnika(1);
      dodajanjeUporabnika(2);
      dodajanjeUporabnika(3);
      setTimeout(meri, 500);
    }
    
    


    
   function generirajPodatke(stPacienta) {
  ehrId = "";
  dodajanjeUporabnika(stPacienta);
 
  return ehrId;
}
































  function pridobiPodatke(){
      var sessionId = getSessionId();
      var ehrId = $("#primerjajEHR").val();
      $("#ITM").html("<b>Racunam itm...</b>");

      var masa;
      var visina; 

      $.ajax({
        url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
        success: function(data){
          var party = data.party;
           $("#primerjajSporocilo").html("<b>Uspesno prebrano</b>");

          //teza
          $.ajax({
            url: baseUrl + "/view/" + ehrId + "/weight",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function(out){
              if(out != undefined){
                masa = out[0].weight;
                izMasa = masa;
              }else{
                $("#primerjajSporocilo").html("<b>Napaka: Ni vseh podatkov!</b>");
              }

            },
            error: function(err){
              $("#primerjajSporocilo").html("<b>Napaka!</b>");
            }
          });
          
          
          
          //visina
          $.ajax({
            url: baseUrl + "/view/" + ehrId + "/height",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function(out){
              if(out != undefined){
                console.log(out);
                visina = out[0].height;
                izVisina = visina;
                    setTimeout(izracunITM, 500);

              }else{
                $("#primerjajSporocilo").html("<b>Napaka: Ni vseh podatkov!</b>");
              }

            },
            error: function(err){
              $("#primerjajSporocilo").html("<b>Napaka!</b>");
            }
          });
          

        },
        error: function(err){
              $("#primerjajSporocilo").html("<b>Napaka!</b>");
        }
        
      });
      
    }
    
    var izMasa;
    var izVisina;
    
    function izracunITM(){
      var v = izVisina/100;
      var itm = izMasa/(v*v);
       itm = Math.round(itm * 100) / 100;
       $("#ITM").html("<b>ITM izbrane osebe je: " +  itm+"</b>");
       var oseba;
       
       if(itm < 20.35){
         oseba = 1;
       }else if(itm < 25.9){
         oseba = 2;
       }else{
         oseba = 3;
       }
       
       narisiGraf(itm,oseba);
       
      

       var img = document.createElement("img");
        var src = document.getElementById("slika");
        if(src.firstChild){
            src.removeChild(src.firstChild);
          }
          
       if(oseba == 1){
                 img.src = "AJ.jpg";
                 $("#naslovSlike").html("<b>ITM imate podoben kot Angelina Jolie!</b>");
                $("#naslovSlike").html('<a class="twitter" href="https://twitter.com/intent/tweet?text=Moj%20ITM%20je%20podoben%20ITM-ju%20od%20Angeline Joulie%21&hashtags=selebritiITM" data-size="large"><img src="https://static.addtoany.com/images/blog/tweet-button-2015.png" style="width:100px"></a>');

       }else if(oseba == 2){
                  img.src = "BE.jpg";
                  $("#naslovSlike").html("<b>ITM imate podoben kot Beyonce!</b>");
                  $("#naslovSlike").html('<a class="twitter" href="https://twitter.com/intent/tweet?text=Moj%20ITM%20je%20podoben%20ITM-ju%20od%20Beyonce%21&hashtags=selebritiITM" data-size="large"><img src="https://static.addtoany.com/images/blog/tweet-button-2015.png" style="width:100px"></a>');


       }else{
                  img.src = "AS.jpg";

                  $("#naslovSlike").html("<b>ITM imate podoben kot Arnold Schwarzenegger!</b>");
                  $("#naslovSlike").html('<a class="twitter" href="https://twitter.com/intent/tweet?text=Moj%20ITM%20je%20podoben%20ITM-ju%20od%20Arnolda Schwarzeneggerja%21&hashtags=selebritiITM" data-size="large"><img src="https://static.addtoany.com/images/blog/tweet-button-2015.png" style="width:100px"></a>');

       }

        src.appendChild(img);
        
    }
    
    
    
    
    
function koperajEHR1(index){
  if(index == 0){
        $("#primerjajEHR").val(''); 

  }
   if(index == 1){
         $("#primerjajEHR").val(eth1); 
   }else if(index == 2){
         $("#primerjajEHR").val(eth2); 

   }else if(index == 3){
         $("#primerjajEHR").val(eth3); 

   }
 }
 
 
 
 
 
 function narisiGraf(num,znanaOseba){
$(function() {
	var data = [num];
	var osebaITM = [];
	var osebaIME;
	if(znanaOseba == 1){
	  //itm do 20,35
	  osebaITM[0] = 17,9;
	  osebaIME ="Angelina Jolie";
	}else if(znanaOseba == 2){
	  //itm do 25,9
	  osebaITM[0] = 21;
	  osebaIME ="Beyonce";
	}else if(znanaOseba == 3){
	  //itm nad 25,9
	  osebaITM[0] =30,8;
	  osebaIME = "Arnold Schwarzenegger";
	}
	
        $('#graf').highcharts({
		chart: {
			type: 'bar'
		},
            title: {
                text: 'ITM',
                x: -20 
            },
            xAxis: {
                categories: ['vrednost',
                ]
            },
            yAxis: {
		max: 40,
		min: 0,
                title: {
                    text: 'ITM'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#606060'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'ITM izbranega',
                data: data
            },{
              name: osebaIME,
              data: osebaITM
            }]
        });
    });
}